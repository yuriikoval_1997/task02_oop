package chemsitry_market;

public enum HouseholdChemistryTypes {
    DISHWASHING_LIQUID,
    LIQUID_LAUNDRY_DETERGENT,
    LIQUID_OVEN_CLEANER,
    SOLID_LAUNDRY_DETERGENT,
    SOLID_OVEN_CLEANER,
    WINDOW_CLEANER
}
