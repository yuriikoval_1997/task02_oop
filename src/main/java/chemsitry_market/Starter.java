package chemsitry_market;


import chemsitry_market.storage.Manager;

public class Starter {
    public static void main(String[] args) {
        Manager.addNewHouseholdProduct(HouseholdChemistryTypes.LIQUID_LAUNDRY_DETERGENT, "REX", 10,
                1000343, 1000, 5);
        Manager.addNewHouseholdProduct(HouseholdChemistryTypes.LIQUID_LAUNDRY_DETERGENT, "MAX", 15,
                6758467, 1000, 7);
        Manager.addNewHouseholdProduct(HouseholdChemistryTypes.LIQUID_OVEN_CLEANER, "CleanPro", 71,
                23415768, 500, 9);
        Manager.addNewHouseholdProduct(HouseholdChemistryTypes.DISHWASHING_LIQUID, "Gala", 3,
                12342435, 500, 20);
        Manager.addNewHouseholdProduct(HouseholdChemistryTypes.SOLID_LAUNDRY_DETERGENT, "REX", 20,
                524331234, 2000, 7);
        Manager.addNewHouseholdProduct(HouseholdChemistryTypes.SOLID_LAUNDRY_DETERGENT, "Ariel", 50,
                5433423, 2000, 8);
        Manager.addNewHouseholdProduct(HouseholdChemistryTypes.WINDOW_CLEANER, "CleanWin", 11,
                843239587, 1000, 6);
        Manager.addNewHouseholdProduct(HouseholdChemistryTypes.SOLID_OVEN_CLEANER, "CleanPro", 61,
                12675487, 500, 13);
    }
}
