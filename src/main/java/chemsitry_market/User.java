package chemsitry_market;

import chemsitry_market.storage.Manager;

public class User {
    private String name;

    public User(String name){
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void findProduct(String name){
        Manager.findProduct(name);
    }
}
