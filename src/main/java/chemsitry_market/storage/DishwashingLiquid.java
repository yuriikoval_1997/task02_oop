package chemsitry_market.storage;

public class DishwashingLiquid extends HouseholdChemistry implements Liquid{
    DishwashingLiquid(String name, int price, int barcode, int quantity) {
        super(name, price, barcode, quantity);
    }

    public int getVolume() {
        return super.getQuantity();
    }
}
