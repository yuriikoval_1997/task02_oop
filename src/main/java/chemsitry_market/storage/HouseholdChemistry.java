package chemsitry_market.storage;

public abstract class HouseholdChemistry extends Product{
    HouseholdChemistry(String name, int price, int barcode, int quantity) {
        super(name, price, barcode, quantity);
    }
}
