package chemsitry_market.storage;

/**
 * This interface shows that product you work with is liquid.
 */
public interface Liquid {
    /**
     * @return volume of the product
     */
    int getVolume();
}
