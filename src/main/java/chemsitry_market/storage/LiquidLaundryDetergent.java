package chemsitry_market.storage;

public class LiquidLaundryDetergent extends HouseholdChemistry implements Liquid {
    LiquidLaundryDetergent(String name, int price, int barcode, int quantity) {
        super(name, price, barcode, quantity);
    }

    public int getVolume() {
        return super.getQuantity();
    }
}
