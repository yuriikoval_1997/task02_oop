package chemsitry_market.storage;

public class LiquidOvenCleaner extends HouseholdChemistry implements Liquid {
    LiquidOvenCleaner(String name, int price, int barcode, int quantity) {
        super(name, price, barcode, quantity);
    }

    @Override
    public int getVolume() {
        return super.getQuantity();
    }
}
