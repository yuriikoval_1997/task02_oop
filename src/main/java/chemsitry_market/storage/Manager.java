package chemsitry_market.storage;

import chemsitry_market.HouseholdChemistryTypes;

import java.util.*;
import java.util.stream.Stream;

public class Manager {
    private static TreeMap<HouseholdChemistry, Integer> householdChemistryProducts;

    private Manager(){}

    public static void addNewHouseholdProduct(HouseholdChemistryTypes TYPE,
                                              String name, int price, int barcode, int quantity,
                                              int numberOfProduct) throws IllegalArgumentException{
        if (numberOfProduct <= 0 || price <= 0) throw new IllegalArgumentException();
        switch (TYPE){
            case WINDOW_CLEANER:
                WindowCleaner wc = new WindowCleaner(name, price, barcode, quantity);
                addToMap(wc, numberOfProduct);
                break;
            case DISHWASHING_LIQUID:
                DishwashingLiquid dl = new DishwashingLiquid(name, price, barcode, quantity);
                addToMap(dl, numberOfProduct);
                break;
            case SOLID_OVEN_CLEANER:
                SolidOvenCleaner soc = new SolidOvenCleaner(name, price, barcode, quantity);
                addToMap(soc, numberOfProduct);
                break;
            case LIQUID_OVEN_CLEANER:
                LiquidOvenCleaner loc = new LiquidOvenCleaner(name, price, barcode, quantity);
                addToMap(loc, numberOfProduct);
                break;
            case SOLID_LAUNDRY_DETERGENT:
                SolidLaundryDetergent sld = new SolidLaundryDetergent(name, price, barcode, quantity);
                addToMap(sld, numberOfProduct);
                break;
            case LIQUID_LAUNDRY_DETERGENT:
                LiquidLaundryDetergent lld = new LiquidLaundryDetergent(name, price, barcode, quantity);
                addToMap(lld, numberOfProduct);
                break;
            default:
                throw new IllegalArgumentException();
        }
    }

    private static void addToMap(HouseholdChemistry  product, int n){
        householdChemistryProducts.put(product,
                householdChemistryProducts.getOrDefault(product, n) + n);
    }

    public static void printMapSortedByName() {
        Comparator<HouseholdChemistry> comparator = Comparator.comparing(Product::getName);
        TreeMap<HouseholdChemistry, Integer> sorted = new TreeMap<>(comparator);
        householdChemistryProducts.forEach(sorted::put);
        sorted.forEach((k, v) -> System.out.println(k.toString()));
    }

    public static void sortedByPrice() {
        Comparator<HouseholdChemistry> comparator = Comparator.comparing(Product::getPrice);
        TreeMap<HouseholdChemistry, Integer> sorted = new TreeMap<>(comparator);
        householdChemistryProducts.forEach(sorted::put);
        sorted.forEach((k, v) -> System.out.println(k.toString()));
    }

    public static HouseholdChemistry findProduct(String productName){
        for (Map.Entry<HouseholdChemistry, Integer> entry : householdChemistryProducts.entrySet()){
            if (entry.getKey().getName().equals(productName)){
                return entry.getKey();
            }
        }
        throw new NoSuchElementException();
    }
}
