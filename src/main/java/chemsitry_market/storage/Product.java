package chemsitry_market.storage;

import java.util.Objects;

abstract class Product implements Comparable<HouseholdChemistry>{
    private static int leftInStorage;
    private String name;
    private int price;
    private int barcode;
    private int quantity;

    static int getLeftInStorage() {
        return leftInStorage;
    }

    static void setLeftInStorage(int leftInStorage) {
        Product.leftInStorage = leftInStorage;
    }

     Product(String name, int price, int barcode, int quantity) {
         this.name = name;
         this.price = price;
         this.barcode = barcode;
         this.quantity = quantity;
     }

    public int getQuantity() {
        return quantity;
    }

    void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String getName() {
        return name;
    }

    public int getPrice() {
        return price;
    }

    void setPrice(int price) {
        this.price = price;
    }

    public int getBarcode() {
        return barcode;
    }

    Product sell(){
        return this;
    }

    @Override
    public int compareTo(HouseholdChemistry householdChemistry) {
        return this.getBarcode() - householdChemistry.getBarcode();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Product product = (Product) o;
        return price == product.price &&
                barcode == product.barcode &&
                quantity == product.quantity &&
                Objects.equals(name, product.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, price, barcode, quantity);
    }

    @Override
    public String toString() {
        return "Product{" +
                "name='" + name + '\'' +
                ", price=" + price +
                ", barcode=" + barcode +
                ", quantity=" + quantity +
                '}';
    }
}