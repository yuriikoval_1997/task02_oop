package chemsitry_market.storage;

/**
 * This interface shows that the product is solid
 */
public interface Solid {
    /**
     * @return weight of the product
     */
    int getWeight();
}
