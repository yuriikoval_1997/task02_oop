package chemsitry_market.storage;

public class SolidLaundryDetergent extends HouseholdChemistry implements Solid{
    SolidLaundryDetergent(String name, int price, int barcode, int quantity) {
        super(name, price, barcode, quantity);
    }

    public int getWeight() {
        return super.getQuantity();
    }
}
