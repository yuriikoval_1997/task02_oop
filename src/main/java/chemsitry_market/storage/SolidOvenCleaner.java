package chemsitry_market.storage;

public class SolidOvenCleaner extends HouseholdChemistry implements Solid {
    SolidOvenCleaner(String name, int price, int barcode, int quantity) {
        super(name, price, barcode, quantity);
    }

    @Override
    public int getWeight() {
        return super.getQuantity();
    }
}
