package chemsitry_market.storage;

public class WindowCleaner extends HouseholdChemistry implements Liquid {
    WindowCleaner(String name, int price, int barcode, int quantity) {
        super(name, price, barcode, quantity);
    }

    public int getVolume() {
        return super.getQuantity();
    }
}
